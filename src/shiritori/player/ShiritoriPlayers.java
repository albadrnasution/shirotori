/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import java.util.ArrayList;
import java.util.List;

/**
 * List of players played in the game.
 *
 * @author albadr.ln
 */
public class ShiritoriPlayers {

    private final List<Player> playersList = new ArrayList<>();

    /**
     * Default number of player is two.
     */
    private int numberOfPlayer = 2;

    /**
     * Setup the players using default option: 2 players, both human.
     */
    public ShiritoriPlayers() {
        this.init();
    }

    public ShiritoriPlayers(int nPlayer) {
        this.numberOfPlayer = nPlayer;
        this.init();
    }

    /**
     * Make the list empty
     */
    private void clear() {
        this.playersList.clear();
    }

    /**
     * Initialize players list with N players, default human.
     */
    private void init() {
        this.clear();
        for (int i = 1; i <= this.count(); ++i) {
            Player p = new HumanPlayer();
            this.playersList.add(p);
        }
    }

    /**
     *
     * @param playerIndex Starts from 1 until N. N is number number of players.
     * @param player Player that is added.
     */
    public void set(int playerIndex, Player player) {
        if (0 < playerIndex && playerIndex <= this.count()) {
            this.playersList.set(playerIndex - 1, player);
        }
    }

    /**
     * Return the player.
     *
     * @param playerIndex Starts from 1 until N. N is total number of players.
     * @return
     */
    public Player get(int playerIndex) {
        if (0 < playerIndex && playerIndex <= this.count()) {
            return this.playersList.get(playerIndex - 1);
        }
        return null;
    }

    /**
     * Change the number of players to play this game. All players will be
     * reverted to default player (Human).
     *
     * @param numberOfPlayers
     */
    public void resize(int numberOfPlayers) {
        this.numberOfPlayer = numberOfPlayers;
        this.init();
    }

    /**
     * Change the number of players to play this game.
     *
     * @return Number of Players
     */
    public int count() {
        return this.numberOfPlayer;
    }

    /**
     * Send observation to all players.
     *
     * @param turnNumber
     * @param turnPlayer
     * @param response
     * @param evaluation
     */
    public void observe(int turnNumber, int turnPlayer, String response, int evaluation) {
        for (int pi = 1; pi <= this.count(); ++pi) {
            this.get(pi).observe(turnNumber, turnPlayer, response, evaluation);
        }
    }
}
