/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import shiritori.dict.Dict;

/**
 * A quite smart AI. Its strategy is to return a response that has hard 
 * last letter, such as v, w, q, x, y, and z.
 * @author albadr.ln
 */
public class AgentNerd extends AgentSF {
    
    // Probability to return the nerd words (ends with xyzwv)
    public double nerdity = 0.75;
    
    // Saving the list upon filtering, so that it is faster in the future
    Map <Character, List<String>> filteredSortedDict = new HashMap<>();
    
    public AgentNerd(Dict dict) {
        super(dict);
    }
    
    private final String[] curseWordOnLose = {"argghhh...", "bleehhhgrgz", "teachMEmaster", 
        "imposibrruu", "OMIGOTO", "igaveup", "ouch", "damn!", "TT", "all words has been used darn it"};
    
    /**
     * Return response word starting with last letter from chain word
     * that has difficult ending letter:  v, w, q, x, y, and z. It can
     * return normal words to. The probability is set in "nerdity" property.
     * @param lastChain The last valid move from other player.
     * @return The move to be submitted by me in response to the last move.
     */
    @Override
    public String respond(String lastChain) {
        char lastChar = lastChain.toLowerCase().charAt(lastChain.length()-1);
        
        List<String> candidates = new LinkedList<>();
        // save the filter if it is new, if not use the old one
        if (filteredSortedDict.containsKey(lastChar)){
            candidates = filteredSortedDict.get(lastChar);
        }else{
            Set<String> wordSet = dict.filterWordsStartingWith(lastChar);
            candidates = new LinkedList<>(wordSet);
            filteredSortedDict.put(lastChar, candidates);
        }
        
        // Filter word from candidates, fing word ends with hard letter
        List<String> hardCandidates = new LinkedList<>();
        candidates.stream().filter(
                (word) -> 
                (word.endsWith("x") || word.endsWith("y") || word.endsWith("z") 
                        || word.endsWith("v") || word.endsWith("w") || word.endsWith("q"))
        ).forEach((word) -> {
            hardCandidates.add(word);
        });
        
        String challenge = "";
        Random rand = new Random(System.currentTimeMillis());
        do{
            if (rand.nextDouble() < this.nerdity && hardCandidates.size() > 0){
                challenge = hardCandidates.get(rand.nextInt(hardCandidates.size()));
                hardCandidates.remove(challenge);
            } 
            else if (candidates.size() > 0){
                challenge = candidates.get(rand.nextInt(candidates.size()));
                candidates.remove(challenge);
            }else{
                int d = rand.nextInt(curseWordOnLose.length);
                challenge = curseWordOnLose[d];
                break;
            }
        }
        while( this.myHistory.contains(challenge) || 
                this.allPlayersHistory.contains(challenge));

        this.myHistory.add(challenge);
        return challenge;
    }
    
}
