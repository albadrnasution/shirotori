/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import shiritori.dict.Dict;

/**
 * This is a dumb implementation of AI. It only returns the reverse of 
 * last valid chain word.
 * @author albadr.ln
 */
public class AgentDumb extends AgentSF {

    public AgentDumb(Dict dict) {
        super(dict);
    }
    
    /**
     * Dumb AI response: reverse of last valid chain.
     * @param lastChain The last valid move from other player.
     * @return The move to be submitted by me in response to the last move.
     */
    @Override
    public String respond(String lastChain){
        String thought = new StringBuilder(lastChain).reverse().toString();
        return thought;
    }
    
}
