/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;
import shiritori.dict.Dict;
import shiritori.dict.DictText;
import shiritori.game.ShiritoriRules;

/**
 * This is an agent undercover in faraway place. She can help you by using her
 * wisdom obtained through her long service in the field.
 *
 * @author albadr.ln
 */
public class AgentWeb extends Agent {

    URL webPath;

    /**
     * A agent on the web implementation as a proof of concept for AI
     * implementation that is not delivered in the bundle. Beware, this is
     * experimental.
     *
     * @param dict Just like normal agent, the agent must know the dictionary
     * used in the game
     * @param url The location of agent.
     */
    public AgentWeb(Dict dict, URL url) {
        super(dict);
        this.webPath = url;
        //send agent the dictionary
        URLConnection con;
        try {
            con = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) con;
            http.setRequestMethod("POST"); // PUT is another valid option
            http.setDoOutput(true);

            Map<String, String> arguments = new HashMap<>();
            arguments.put("method", "init");
            arguments.put("gameid", "root");
            arguments.put("dict", this.dict.toJSON());

            StringJoiner sj = new StringJoiner("&");
            for (Map.Entry<String, String> entry : arguments.entrySet()) {
                sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
                        + URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
            int length = out.length;
            http.setFixedLengthStreamingMode(length);
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            http.connect();
            try (OutputStream os = http.getOutputStream()) {
                os.write(out);
            }
            // Do Something With Returned Response
            // TODO: The protocol is not fixed yet!
            try (BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()))) {
                String line;
                StringBuilder sb = new StringBuilder();
                // For now let just print the response
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                System.out.println(sb.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(AgentWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Send the lastChain to the agent URL and wait for her response.
     *
     * @param lastChain
     * @return
     */
    @Override
    public String respond(String lastChain) {
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(this.webPath + "?method=respond&chain=" + lastChain);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();

            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                return sb.toString();
            }
        } catch (IOException ex) {
            Logger.getLogger(AgentWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * Send agent in the field (web) about the last move in the game.
     *
     * @param turn The turn of this observation.
     * @param player The player in which the turn is happening.
     * @param response The response submitted by player. Not necessarily a valid
     * chain.
     */
    @Override
    public void observe(int turn, int player, String response, int evaluation) {
        //send info to the web
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(this.webPath + "?method=observe&turn=" + turn
                    + "&player=" + player + "&response=" + response);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()))) {
                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                String responseFromAgent = sb.toString();
                // Do something with the response from agent on the field
            }
        } catch (IOException ex) {
            Logger.getLogger(AgentWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void contact() {

    }

    @Override
    public void learn(ShiritoriRules rules) {
    }

    /**
     * Testing connection of this prototype.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Dict dict = new DictText();
            AgentWeb dumbFieldAgent = new AgentWeb(dict, new URL("http://localhost/shiritori/dumb.php"));
            // Should return "tset"
            System.out.println(dumbFieldAgent.respond("test"));
        } catch (MalformedURLException ex) {
            Logger.getLogger(AgentWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
