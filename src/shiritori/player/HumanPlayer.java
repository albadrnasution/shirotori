/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import shiritori.game.ShiritoriRules;

/**
 * This is a human player, which means it get the response from the user playing
 * the game. It will ask user to put text, whether it is from console or any
 * other input stream that is determined.
 *
 * @author albadr.ln
 */
public class HumanPlayer extends Player implements PlayerInterface {

    InputStream is;

    /**
     * By default, use console.
     */
    public HumanPlayer() {
        this.is = System.in;
    }

    /**
     * Not default? Use whatever input stream you want to use.
     * @param is 
     */
    public HumanPlayer(InputStream is) {
        this.is = is;
    }

    /**
     * Get respond from user by input stream. By default the input stream is
     * from standard input, i.e. from console.
     *
     * @param lastChain The last valid move from other player.
     * @return The move to be submitted by me in response to the last move.
     */
    @Override
    public String respond(String lastChain) {

        // Get console
        try {
            // Get the object of DataInputStream
            InputStreamReader isr = new InputStreamReader(this.is);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            if ((line = br.readLine()) != null) {
                return line;
            }
            isr.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return "";

        //String input = System.console().readLine();
        //return input;
    }

    /**
     * This is human. Nothing to do here
     */
    @Override
    public void observe(int turn, int player, String response, int evaluation) {
        // Hey human, please remember it by yourself.
    }

    /**
     * This is human. Nothing to do here
     *
     * @param rules Rules to be remembered.
     */
    @Override
    public void learn(ShiritoriRules rules) {
        // Please learn it by yourself. You are human after all.
    }

}
