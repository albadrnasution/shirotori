/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import shiritori.game.ShiritoriRules;

/**
 * Interface of Shiritori players. Methods will be called by ShiritoriEngine
 * automatically.
 * @author albadr.ln
 */
public interface PlayerInterface {

    /**
     * To inform interested player about the rules currently in place. This
     * method will be called exactly once at the beginning of the game,
     *
     * @param rules Rules of the game.
     */
    public void learn(ShiritoriRules rules);

    /**
     * Get respond from this player. This method will be called by the engine on
     * this player's turn.
     *
     * @param lastResponse The last move from other player.
     * @return The move to be submitted by me in response to the last move.
     */
    public String respond(String lastResponse);

    /**
     * Method to tell this player the latest move and if necessary record it.
     * This will be called on every turn, right after a player in the game is
     * finished evaluated.
     *
     * @param turn The turn of this observation.
     * @param player The player in which the turn is happening. Possibly me.
     * @param response The response submitted by player. Not necessarily a valid
     * chain.
     * @param evaluation The evaluation result of the response.
     */
    public void observe(int turn, int player, String response, int evaluation);
}
