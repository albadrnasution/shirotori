/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import shiritori.dict.Dict;
import shiritori.game.ShiritoriRules;

/**
 *
 * One of AI implementation for Shiritori. This AI receive dictionary
 * and can find response from the directory.
 * @author albadr.ln
 */
public abstract class Agent extends Player implements PlayerInterface {
    
    Dict dict;
    ShiritoriRules rules;
    
    public Agent(Dict dict){
       this.dict = dict;
    }

    /**
     * Save the rules of the game. This method will be called exactly once 
     * at the beginning of the game.
     * @param rules Rules of the game.
     */
    @Override
    public void learn(ShiritoriRules rules) {
        this.rules = rules;
    }

    /**
     * Get respond from player. This will be called by the engine on this 
     * player's turn.
     * @param lastResponse The last move from other player.
     * @return The move to be submitted by me in response to the last move.
     */
    @Override
    public abstract String respond(String lastResponse);
    
    /**
     * Method to tell this player the latest move and if necessary 
     * record it.
     * @param turn The turn of this observation.
     * @param player The player in which the turn is happening.
     * @param response The response submitted by player. Not necessarily a valid chain.
     * @param evaluation Evaluation result, with rule index being violated if non-negative.
     */
    @Override
    public abstract void observe(int turn, int player, String response, int evaluation);

}
