/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import shiritori.dict.Dict;
import shiritori.game.ShiritoriRules;

/**
 * One of AI implementation for Shiritori. This AI receive dictionary and can
 * find response from the directory.
 *
 * @author albadr.ln
 */
public class AgentSF extends Agent {

    Map <Character, List<String>> filteredSortedDict = new HashMap<>();

    public AgentSF(Dict dict) {
        super(dict);
    }

    /**
     * Return response of this AI. The implementation of this AI is quite
     * naive/simple, it only return the first occurring words in dictionary that
     * starts with the last letter of lastChain word and short enough so that
     * the enemy human player have a hard time.
     *
     * @param lastChain Last valid move
     * @return Our response.
     */
    @Override
    public String respond(String lastChain) {
        char lastChar = lastChain.toLowerCase().charAt(lastChain.length() - 1);
        List<String> candidates = new LinkedList<>();
        // save the filter if it is new, if not use the old one
        if (!filteredSortedDict.containsKey(lastChar)){
            Set<String> wordSet = dict.filterWordsStartingWith(lastChar);
            candidates = new LinkedList<>(wordSet);    
            filteredSortedDict.put(lastChar, candidates);
        }else{
            candidates = filteredSortedDict.get(lastChar);
        }
        
        java.util.Collections.sort(candidates, (String o1, String o2) -> o1.length() - o2.length());
        String challenge = candidates.size() > 0 ? candidates.get(0) : "";
        Random rand = new Random(System.currentTimeMillis());
        
        int iter = 0;
        do {
            if (candidates.size() > 0) {
                challenge = candidates.get(iter);
                iter++;

                if (iter >= candidates.size()) {
                    break;
                }
            } else {
                break;
            }
        } while (this.myHistory.contains(challenge)
                || this.allPlayersHistory.contains(challenge));
        
        // Remove of the record so that future search faster
        candidates.remove(challenge);

        // Record my own history for future reference
        myHistory.add(challenge);
        return challenge;
    }

    /**
     * Save the player id and response to the my repository. This method 
     * should be called by the engine every time a turn is finished.
     *
     * @param turn The turn in which this observation happened.
     * @param player Index of player, possibly me.
     * @param response The response submitted by player. Not necessarily a valid
     * chain.
     * @param evaluation The evaluation result. Non-negative if it is rule
     * violation, representing the index of rule being violated.
     */
    @Override
    public void observe(int turn, int player, String response, int evaluation) {
        if (evaluation == ShiritoriRules.SUCCESS_CHAINING) {
            allPlayersHistory.add(response);
            allPlayersIdxHistory.add(player);
        }
    }
    
}
