/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import shiritori.game.ShiritoriEngine;
import shiritori.dict.Dict;
import shiritori.dict.DictText;
import shiritori.game.ShiritoriRules;
import shiritori.player.AgentDumb;
import shiritori.player.AgentNerd;
import shiritori.player.AgentSF;
import shiritori.player.AgentWeb;
import shiritori.player.HumanPlayer;
import shiritori.player.ShiritoriPlayers;

/**
 * This is a control portal implemented as console program for the Shiritori
 * game.
 *
 * @author albadr.ln
 */
public class Shiritori {

    /**
     * Just a helper to print dictionary, or sample of it beautifully.
     *
     * @param dict
     */
    public static void printDict(Dict dict) {
        Set<String> sample = dict.getSampleWords();
        int nword = 0;
        for (String w : sample) {
            int nspace = 18 - w.length();
            System.out.print(" " + w + " ");
            for (int i = 0; i < nspace; ++i) {
                System.out.print(" ");
            }
            nword++;
            if (nword % 4 == 0) {
                System.out.println();
            }
        }
    }

    /**
     * Just a helper to print chain beautifully.
     *
     * @param chains
     */
    public static void printChain(List<String> chains) {
        int sumLength = 0;
        for (String w : chains) {
            System.out.print(w + " > ");
            sumLength += w.length() + 3;
            if (sumLength > 70) {
                System.out.println();
                System.out.print("... ");
                sumLength = 0;
            }
        }
    }

    /**
     * Shiritori Game in a console portal.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Message: Well... You know... Start screen.
        System.out.println("  __                        ");
        System.out.println(" (_ |_  o  __ o _|_ _  __ o ");
        System.out.println(" __)| | |  |  |  |_(_) |  | ");
        System.out.println();
        System.out.println("Welcome. This is a console portal of Shiritori Game.");
        System.out.println("Created by Albadr Lutan Nasution.");
        System.out.println();

        // Message: Prompt for custom dictionary
        System.out.println("Please enter dictionary you want to use. ");
        System.out.println("If you enter invalid path, default English corpus will be used. ");
        System.out.print("Path / URL : ");
        String dictionaryPath = System.console().readLine();

        // Setup: Load dictionary
        Dict candidates = new DictText();
        Dict allEnglish = new DictText();
        Dict indonesia = new DictText();
        try {
            allEnglish = new DictText("Dict/Dict.en.txt");
            indonesia = new DictText("Dict/Dict.id.txt");
            //check if it is an URL
            if (dictionaryPath.startsWith("http://") 
                    || dictionaryPath.startsWith("https://")
                    || dictionaryPath.startsWith("ftp://")
                    || dictionaryPath.startsWith("file://")) {
                candidates = new DictText(new URI(dictionaryPath));
            } else {
                candidates = new DictText(dictionaryPath);
            }
        } catch (IOException ex) {
            candidates = allEnglish;
            Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("English corpus is used.\n");
        } catch (URISyntaxException ex) {
            candidates = allEnglish;
            Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Setup: Create engine
        ShiritoriEngine engine = new ShiritoriEngine(candidates);
        ShiritoriRules rules = ShiritoriRules.english();
        ShiritoriPlayers players = new ShiritoriPlayers();

        // Message: Prompt for asking user about player count and configuration
        int nplayer = 2;
        System.out.print("Please enter number of players (at least 2): ");
        String nplayerString = System.console().readLine();
        try {
            nplayer = Integer.parseInt(nplayerString);
        } catch (Exception ex) {
            Logger.getLogger(Shiritori.class.getName()).log(Level.WARNING, null, ex);
            System.out.println("Invalid number is given. Two players will be used.");
        }

        // Setup: Players count and configuration
        players.resize(nplayer);
        Random rand = new Random(System.currentTimeMillis());

        for (int pid = 1; pid <= nplayer; pid++) {
            String hora = "";
            do {
                System.out.print("Player " + pid + ", human (h) or AI (a [0-2]): ");
                hora = System.console().readLine();
                if (!(hora.startsWith("a") || hora.startsWith("h"))) {
                    System.out.println("Please enter h for human or a for AI.");
                    System.out.println("By the way, the AI level 0 is dumb, 1 normal and 2 annoying.");
                }
            } while (!(hora.startsWith("a") || hora.startsWith("h")));

            // Configurinf AI
            if (hora.startsWith("a")) {
                String[] horaSplit = hora.split(" ");
                // Well, there is some AI 
                if (horaSplit.length == 1) {
                    if (rand.nextDouble() < 0.5) {
                        players.set(pid, new AgentNerd(candidates));
                    } else {
                        players.set(pid, new AgentSF(candidates));
                    }
                } else if (horaSplit[1].startsWith("0")) {
                    players.set(pid, new AgentDumb(candidates));
                } else if (horaSplit[1].startsWith("1")) {
                    players.set(pid, new AgentSF(candidates));
                } else if (horaSplit[1].startsWith("2")) {
                    players.set(pid, new AgentNerd(candidates));
                } else if (horaSplit[1].startsWith("http://") || horaSplit[1].startsWith("https://")){
                    // A web AI! Let's set it up
                    URL aiURL;
                    try {
                        aiURL = new URL(horaSplit[1]);
                        players.set(pid, new AgentWeb(candidates, aiURL));
                    } catch (MalformedURLException ex) {
                        players.set(pid, new AgentSF(candidates));
                        Logger.getLogger(Shiritori.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else{
                    players.set(pid, new AgentSF(candidates));
                }
            }
        }

        // Message: Print dictionary
        System.out.println("Just some samples of candidates in dictionary (max 100 of them):");
        Shiritori.printDict(candidates);

        // FINISH SETUP AND START PLAYING
        engine.setup(rules, players);

        // Message: printing initial word
        String prevWord = engine.getStates().getInitialWord();
        System.out.println();
        System.out.println("INIT> " + prevWord);
        
        // RUN THE GAME!
        while (engine.getStates().stillPlaying()) {

            // Message: prompt for notifying console user for turn and current player
            int pi = engine.getStates().whoIsPlaying();
            int currentTurn = engine.getStates().getCurrentTurnCount();
            System.out.print("[TURN " + currentTurn + "] ");
            Shiritori.printChain(engine.getStates().getChain(5));
            System.out.print("(P" + pi + ") ");

            // EXECUTE THIS TURN
            int evaluation = engine.execute();

            // Message: printing response from no human player
            if (!(engine.getStates().getLastSubmittingPlayer() instanceof HumanPlayer)) {
                System.out.println(engine.getStates().getLastSubmittedWord());
            }

            // Message: notifyinf a losing player
            if (evaluation >= 0) {
                String invMsg = rules.get(evaluation).invalidMessage();
                System.out.print("Player " + pi + " submitted '" + engine.getStates().getLastSubmittedWord());
                System.out.println("' and LOSE: " + invMsg);
            }
        }

        // Write report of the game
        System.out.println();
        System.out.println("The trails of this game:");
        Shiritori.printChain(engine.getStates().getChain());
        System.out.println("(GAME OVER)");
        System.out.println();
        System.out.println("WINNER: P" + engine.getStates().whoWins());
        System.out.println();
        // Last report for every players
        for (int pIdx = 1; pIdx <= players.count(); ++pIdx) {
            int violation = engine.getStates().getPlayerWinStatus_RuleViolation(pIdx);
            if (violation >= 0) {
                String invMsg = rules.get(violation).invalidMessage();
                System.out.println("P" + pIdx + " LOSE at turn " + engine.getStates().getPlayerWinStatus_Turn(pIdx) + ": " + invMsg);
            } else {
                System.out.println("P" + pIdx + " IS THE BOSS!");
            }
        }
    }

}
