/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

import java.util.ArrayList;
import java.util.List;

/**
 * This rule will evaluate if the input already submitted by currently playing
 * player in this turn.
 *
 * @author albadr.ln
 */
public class RuleNoDoublePersonal extends GameRule {

    /**
     * @param input
     * @return True if input has not been submitted by currently playing player
     * before.
     */
    @Override
    public boolean evaluate(String input) {
        List<String> playerHist = gs.getPlayersHistory();
        List<Integer> playerIdxHist = gs.getPlayersIdxHistory();
        List<String> curPlayHist = new ArrayList<>();
        for (int i = 0; i < playerIdxHist.size(); ++i) {
            if (playerIdxHist.get(i) == gs.getLastSubmittingPlayerIdx()) {
                curPlayHist.add(playerHist.get(i));
            }
        }
        // Check whether this player has submited the input
        return !curPlayHist.contains(input);
    }

    @Override
    public String invalidMessage() {
        return "The player has used the submitted word before.";
    }

}
