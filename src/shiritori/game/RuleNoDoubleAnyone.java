/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

/**
 * This rule will evaluate if the input already submitted by any player in the
 * game.
 *
 * @author albadr.ln
 */
public class RuleNoDoubleAnyone extends GameRule {

    /**
     * Return true is the submitted word is not used by anyone before.
     * @param input
     * @return 
     */
    @Override
    public boolean evaluate(String input) {
        String curWord = input.toLowerCase();
        return !(gs.getPlayersHistory().contains(curWord));
    }

    @Override
    public String invalidMessage() {
        return "Someone used the submitted word before.";
    }

}
