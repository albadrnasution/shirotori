/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

/**
 * Will check whether the input string is chaining, to the last chain. If the
 * last chain is empty string, return true;
 *
 * @author albadr.ln
 */
public class RuleChain extends GameRule {

    /**
     * @param input
     * @return True if last chain is empty string. If not, return true if the
     * last character of last chain is the same as current input.
     */
    @Override
    public boolean evaluate(String input) {
        if (gs.getLastChain().equals("")) {
            return true;
        }
        char lastCharOfPrevWord = gs.getLastChain().charAt(gs.getLastChain().length() - 1);
        char firstCharOfCurWord = input.charAt(0);
        return lastCharOfPrevWord == firstCharOfCurWord;
    }

    @Override
    public String invalidMessage() {
        return "Shiritori chain rule is violated.";
    }

}
