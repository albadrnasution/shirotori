/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

/**
 * This rule will search the dictionary.
 *
 * @author albadr.ln
 */
public class RuleKnownWord extends GameRule {

    /**
     * 
     * @param input
     * @return True if the input is in the dictionary.
     */
    @Override
    public boolean evaluate(String input) {
        // Turn case to small, and check in the dictionary. Assume ascii.
        String curWord = input.toLowerCase();
        // Check word whether exist or not in candidates
        return gs.getDict().contains(curWord);
    }

    @Override
    public String invalidMessage() {
        return "Word is not exist in list of candidates.";
    }
}
