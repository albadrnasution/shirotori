/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

/**
 * Check whether the input consist of alphabet only or not.
 * @author albadr.ln
 */
public class RuleAlphabetValidity extends GameRule {

    /**
     * @param input
     * @return True if input only consist of alphabet, lower or UPPER case.
     */
    @Override  
    public boolean evaluate(String input){
        return input.matches("[a-zA-Z]+");
    }

    @Override
    public String invalidMessage() {
        return "Symbols, number, or non-alphabet characters are submitted.";
    }
    
}
