/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

import shiritori.dict.Dict;
import shiritori.player.Player;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import shiritori.dict.DictText;
import shiritori.player.ShiritoriPlayers;

/**
 * An engine to move the Shiritori game.
 *
 * @author albadr.ln
 */
public class ShiritoriEngine {

    /**
     * Class to record current affairs of the game and to be sent to GameRule
     * for evaluation i.e. on new submission. All property are set to be private
     * for safety, so that only the game engine can change its value.
     */
    public class GameStates {

        private String initialWord;
        private String lastChain;
        private String lastSubmittedWord;
        private int lastSubmittingPlayer;

        /**
         * Return the word that start them all.
         *
         * @return
         */
        public String getInitialWord() {
            return this.initialWord;
        }

        /**
         * Return the latest submitted word that is valid.
         *
         * @return
         */
        public String getLastChain() {
            return this.lastChain;
        }

        /**
         * Return the last submitted word by last playing player, even though it
         * may not be valid chain word.
         *
         * @return
         */
        public String getLastSubmittedWord() {
            return lastSubmittedWord;
        }

        /**
         * Return the index of last player that submit a response. The player
         * may not be in the game anymore.
         *
         * @return
         */
        public int getLastSubmittingPlayerIdx() {
            return lastSubmittingPlayer;
        }

        /**
         * Return the last player that submit a response. The player may not be
         * in the game anymore.
         *
         * @return
         */
        public Player getLastSubmittingPlayer() {
            int idx = this.lastSubmittingPlayer;
            return players.get(idx);
        }

        private Dict dict;

        public Dict getDict() {
            return dict;
        }

        private final List<Integer> allPlayersIdxHistory = new ArrayList<>();
        private final List<String> allPlayersHistory = new ArrayList<>();

        public List<String> getPlayersHistory() {
            return new ArrayList<>(allPlayersHistory);
        }

        public List<Integer> getPlayersIdxHistory() {
            return new ArrayList<>(allPlayersIdxHistory);
        }

        private final Queue<Integer> playQueue = new ArrayDeque<>();
        private final List<Integer> statusPlayersLoseAtTurn = new ArrayList<>();
        private final List<Integer> statusPlayersLoseReason = new ArrayList<>();
        /* Game status: if true, game is not over yet */
        private boolean statusStillPlaying = true;
        /* Game status: now is the i-th turn */
        private int statusTurnCount = 1;

        /**
         * Return the number of turn since the game begin. The first turn is
         * turn number 1. Turn number will be incremented after a player plays.
         *
         * @return Number of turn.
         */
        public int getCurrentTurnCount() {
            return this.statusTurnCount;
        }

        /**
         * Generate a new initial word. This method can only be called by
         * initialize method.
         *
         * @return
         */
        private String generateInitialWord() {
            return this.dict.getSampleWord();
        }

        /**
         * Return chain of submitted word from beginning till end.
         *
         * @return
         */
        public List<String> getChain() {
            return this.getPlayersHistory();
        }

        /**
         * Return the last N submitted word that is valid.
         *
         * @param N Amount of chains.
         * @return
         */
        public List<String> getChain(int N) {
            if (this.allPlayersHistory.size() <= N) {
                return this.getPlayersHistory();
            } else {
                List<String> chains = new ArrayList<>();
                for (int i = 0; i < N; i++) {
                    int idx = this.allPlayersHistory.size() + i - N;
                    chains.add(this.allPlayersHistory.get(idx));
                }
                return chains;
            }
        }

        /**
         * @return Number of remaining player currently.
         */
        public int getPlayersRemainingCount() {
            return this.playQueue.size();
        }

        /**
         * Return the win status or rather lose status of a player.
         *
         * @param playerIdx Index of player, starts from 1.
         * @return The turn in which player lose. Zero if not lose yet.
         */
        public int getPlayerWinStatus_Turn(int playerIdx) {
            if (playerIdx > 0 && playerIdx <= players.count()) {
                return this.statusPlayersLoseAtTurn.get(playerIdx - 1);
            }
            return 0;
        }

        /**
         * Return the status of player. If OK will be negative number. If not,
         * player already lose because of a rule violation.
         *
         * @param playerIdx Index of player, starts from 1.
         * @return Index of rule that is violated by this player, starts from 0.
         */
        public int getPlayerWinStatus_RuleViolation(int playerIdx) {
            if (playerIdx > 0 && playerIdx <= players.count()) {
                return this.statusPlayersLoseReason.get(playerIdx - 1);
            }
            return ShiritoriRules.SUCCESS_CHAINING;
        }

        /**
         * Return the player ID if the game is already ended. Return -1 if the
         * game is till running/winner is not determined yet.
         *
         * @return The last standing player ID.
         */
        public int whoWins() {
            if (this.stillPlaying() || this.getPlayersRemainingCount() > 1) {
                return -1;
            } else if (this.getPlayersRemainingCount() == 1) {
                return this.playQueue.peek();
            }
            return 0;
        }

        /**
         * Return the ID of currently playing player i.e. player that have to
         * submit a word now.
         *
         * @return Playing player.
         */
        public int whoIsPlaying() {
            return this.playQueue.peek();
        }

        /**
         * Return the playing order in queue. The returned queue is a new
         * instance, so that it did not affect current game states.
         *
         * @return
         */
        public Queue getPlayingOrder() {
            return new ArrayDeque<>(this.playQueue);
        }

        /**
         * Return true if the game is still playing, i.e. more than one player
         * exist.
         *
         * @return
         */
        public boolean stillPlaying() {
            return this.statusStillPlaying;
        }

        /**
         * Return false if the game is already over.
         *
         * @return
         */
        public boolean gameOver() {
            return !this.stillPlaying();
        }
    }

    /* Rules of the game */
    ShiritoriRules rules;
    /* Players of the game */
    ShiritoriPlayers players;
    /* State of the game, also be used to evaluate last player's move*/
    GameStates states = new GameStates();

    public GameStates getStates() {
        return states;
    }

    // logger for well... logging
    static final Logger logger = Logger.getLogger(ShiritoriEngine.class.getName());

    /**
     * Using the default dictionary, which is English corpus but if failed to
     * load it will use dummy.
     */
    public ShiritoriEngine() {
        Dict dictionary = new DictText();
        this.initialize(dictionary);
    }

    public ShiritoriEngine(Dict dictionary) {
        this.initialize(dictionary);
    }

    private void initialize(Dict dictionary) {
        this.states.dict = dictionary;
        this.states.initialWord = this.states.generateInitialWord();
        this.states.lastChain = this.states.getInitialWord();
        // default rules: english
        this.rules = ShiritoriRules.english();
        // default players: 2 human player
        this.players = new ShiritoriPlayers(2);
        this.initPlayers();
        // history: by dealer (Player 0)
        this.states.allPlayersHistory.add(this.states.getInitialWord());
        this.states.allPlayersIdxHistory.add(0);
    }

    private void initPlayers() {
        //this.states.playersList.clear();
        this.states.playQueue.clear();
        this.states.statusPlayersLoseAtTurn.clear();
        this.states.statusPlayersLoseReason.clear();
        //at least initialize the players list with normal player
        for (int i = 1; i <= players.count(); ++i) {
            //Player p = new HumanPlayer();
            //this.states.playersList.add(p);
            this.states.playQueue.add(i);
            // init, noone lose, turn = 0
            this.states.statusPlayersLoseAtTurn.add(0);
            // init, noone lose
            this.states.statusPlayersLoseReason.add(ShiritoriRules.SUCCESS_CHAINING);
            // broadcast a COPY of the rules of this game;
            players.get(i).learn(rules);
        }
    }

    /**
     * Do one execution, from feeding current player until evaluation the
     * player's response.
     *
     * @return Evaluation status. Negative means OK. Non-negative means the last
     * response violates rule. The value is the index of rule being violated.
     */
    public int execute() {
        // Take currently playing player
        int playerIdx = this.states.whoIsPlaying();
        Player player = this.players.get(playerIdx);
        this.states.lastSubmittingPlayer = playerIdx;

        // Ask the response
        String curRespond = player.respond(this.states.getLastChain());

        // Record the move to update states before evaluation
        this.states.lastSubmittedWord = curRespond.toLowerCase();

        // Evaluate the move and record chain if all condition valid
        int turnEvaluation = rules.evaluate(this.states);

        logger.log(Level.FINEST, "Player {0} evaluate {1} ", new Object[]{playerIdx, turnEvaluation});

        // Broadcast the move to all players, die or not
        players.observe(this.states.getCurrentTurnCount(), playerIdx, curRespond, turnEvaluation);

        // Player turn is over, remove from queue
        int polledPlayer = this.states.playQueue.poll();
        // Check the removed player and currently playing player just in case
        if (polledPlayer == playerIdx) {
            if (turnEvaluation == ShiritoriRules.SUCCESS_CHAINING) {
                // record current succeed word
                this.states.lastChain = curRespond.toLowerCase();
                // Record the history
                this.states.allPlayersHistory.add(curRespond.toLowerCase());
                this.states.allPlayersIdxHistory.add(playerIdx);

                // If this player survives this turn, put her to the back of play queue
                this.states.playQueue.add(playerIdx);
            } else {
                // record current player losing status
                // the turn he lose and rule violation
                this.states.statusPlayersLoseAtTurn.set(playerIdx - 1, this.states.getCurrentTurnCount());
                this.states.statusPlayersLoseReason.set(playerIdx - 1, turnEvaluation);
            }
        } else if (polledPlayer != playerIdx) {
            // something wrong, polled player is not the playing player
            // put him back to the back and continue game
            this.states.playQueue.add(playerIdx);
            // Logging
            logger.log(Level.INFO, "Player {0} plays but {1} is polled", new Object[]{playerIdx, polledPlayer});
        }

        // If there is only ONE remaining player, game over!
        if (this.states.getPlayersRemainingCount() == 1) {
            this.states.statusStillPlaying = false;
        }

        // If the last player (ID=N) was playing, the turn is counted
        this.states.statusTurnCount++;

        return turnEvaluation;
    }

    /**
     * Setup the rules and players of current Shiritori game. Calling this
     * method tell the engine that it is ready to play the game.
     *
     * @param rules
     * @param players
     */
    public void setup(ShiritoriRules rules, ShiritoriPlayers players) {
        this.rules = rules;
        this.players = players;
        this.initPlayers();
    }

}
