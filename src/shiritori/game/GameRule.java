/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

/**
 *
 * @author albadr.ln
 */
public abstract class GameRule {

    ShiritoriEngine.GameStates gs;

    /**
     * Evaluate current game state. This is called by the engine every time a
     * player submit new word.
     *
     * @param gs
     * @return
     */
    protected boolean evaluate(final ShiritoriEngine.GameStates gs) {
        this.gs = gs;
        return evaluate(gs.getLastSubmittedWord());
    }

    /**
     * Evaluate the input string whether it is valid from the perspective of
     * latest state of the game. This method can be used by player for
     * automatically test their candidate of submission before actually
     * submitting it.
     *
     * @param input
     * @return
     */
    public abstract boolean evaluate(String input);

    /**
     * Invalid message that will be print out when a player violate the rule.
     *
     * @return
     */
    public abstract String invalidMessage();
}
