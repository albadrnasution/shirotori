/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.game;

import java.util.ArrayList;
import java.util.List;
import shiritori.game.ShiritoriEngine.GameStates;

/**
 *
 * @author albadr.ln
 */
public class ShiritoriRules {
    
    public static final int SUCCESS_CHAINING = -1; 
    
    List<GameRule> rules;

    public ShiritoriRules() {
        rules = new ArrayList<>();
    }
    
    public static ShiritoriRules english(){
        ShiritoriRules english = new ShiritoriRules();
        english.resetRule();
        english.addRule(new RuleAlphabetValidity());
        english.addRule(new RuleChain());
        english.addRule(new RuleKnownWord());
        english.addRule(new RuleNoDoublePersonal());
        english.addRule(new RuleNoDoubleAnyone());
        return english;
    }
    
    /**
     * Add new rule to the engine.
     * @param gameRule The rule to be added.
     */
    public void addRule(GameRule gameRule){
        this.rules.add(gameRule);
    }
    
    /**
     * Set certain rule in the index (priority).
     * @param ruleIdx Priority of the rule. Idx 1 will be evaluated first. 
     * @param gameRule The rule to be set.
     */
    public void setRule(int ruleIdx, GameRule gameRule){
        if (0 <= ruleIdx && ruleIdx < this.rules.size()){
            this.rules.set(ruleIdx, gameRule);
        }
    }
    
    /**
     * Delete all rule from this engine.
     */
    public void resetRule(){
        this.rules.clear();
    }
    
    /**
     * Get the rule object.
     * @param ruleIndex Index of the rule, which represent its priority.
     * @return 
     */
    public GameRule get(int ruleIndex){
        if (0 <= ruleIndex && ruleIndex < this.rules.size()){
            return this.rules.get(ruleIndex);
        }
        return null;
    }
    
    /**
     * Evaluate with currentWord the game is continue or not. This method
     * is only evaluating the word and should not change the any game states.
     * @TODO using exception for word not exist in dictionary?
     * @param states latest state of the game
     * @return 
     */
    public int evaluate(GameStates states){
        boolean val = true;
        int violation = ShiritoriRules.SUCCESS_CHAINING;
        for (int r = 0; r < rules.size(); r++){
            val = val && rules.get(r).evaluate(states);
            if (!val) {
                violation = r;
                break;
            }
        }
        return violation;
    }
    
    public int size(){ return this.rules.size(); }
}
