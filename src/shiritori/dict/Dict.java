/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.dict;

import java.util.Set;

/**
 *
 * @author albadr.ln
 */
public interface  Dict  {
        
    /**
     * Check whether this word is present in the dictionary.
     * @param word Word that wants to be searched.
     * @return True if word is present.
     */
    public abstract boolean contains(String word);
    
    /**
     * Return a message containing information about this dictionary.
     * @return Information about this dictionary.
     */
    public abstract String information();
    
    
    /**
     * Give one random word from this dictionary.
     * @return Sample word.
     */
    public abstract String getSampleWord();
    
    /**
     * Give a small set of words containing in this dictionary.
     * @return Sample words.
     */
    public abstract Set<String> getSampleWords();
    
    /**
     * Give all words in this dictionary. If that is not possible, return
     * null.
     * @return Complete words in this dictionary as Set.
     */
    public abstract Set<String> getAllWords();

    /**
     * Return all words in this dictionary that starts with a character.
     * @param firstChar Character the words starts with
     * @return Unique set of words starting with the character.
     */
    public abstract Set<String> filterWordsStartingWith(char firstChar);
    
    /**
     * Return all words in this dictionary that ends with a character.
     * @param lastChar Character the words ends with
     * @return Unique set of words ending with the character.
     */
    public abstract Set<String> filterWordsEndingWith(char lastChar);
    
    /**
     * A JSON from of this dictionary. Can be a list of its words written 
     * in JSON, or detailed information how to access this dictionary members.
     * @TODO Create specification about the information.
     * @return 
     */
    public abstract String toJSON();
}
