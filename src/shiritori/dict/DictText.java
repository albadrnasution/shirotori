/*
 * This is a project to be submitted to Works Applications co.,ltd.
 * for recruitment purpose as Examination 2nd stage.
 * This project is written by candidate: Albadr Lutan Nasution
 * 2016-07-15
 */
package shiritori.dict;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A implementation of word candidates or Dictionary. This implementation
 * assumes that dictionary is saved on a txt file, one word per line.
 *
 * @author albadr.ln
 */
public class DictText implements Dict {

    private Set<String> candidates = new HashSet<>();
    private final String sourcePath;
    private int nSample = 100;

    public DictText() {
        this.sourcePath = "DICT/Dict.en.txt";
        try {
            InputStream is = new FileInputStream(this.sourcePath);
            this.populate(is);
        } catch (FileNotFoundException ex) {
            // if fail to locate the default file, well, populate with
            // default from pdf specification
            this.candidates = new HashSet<>();
            this.candidates.add("internet");
            this.candidates.add("tail");
            this.candidates.add("grep");
            this.candidates.add("less");
            this.candidates.add("telnet");
            this.candidates.add("less");
            this.candidates.add("sed");
            Logger.getLogger(DictText.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DictText.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    public DictText(URI url) throws IOException {
        InputStream is = url.toURL().openStream();
        this.sourcePath = url.getPath();
        this.populate(is);
    }

    public DictText(String filename) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(filename);
        this.sourcePath = filename;
        this.populate(is);
    }

    public DictText(File file) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(file);
        this.sourcePath = file.getPath();
        this.populate(is);
    }

    private void populate(InputStream is) throws IOException {
        this.candidates = new HashSet<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                // add word (one line) to container
                this.candidates.add(line.toLowerCase());
            }
        }
    }

    /**
     * Set the number of sample to be returned arbitrarily.
     *
     * @param newNumberOfSamples
     */
    public void setNumberOfSample(int newNumberOfSamples) {
        this.nSample = newNumberOfSamples;
    }

    /**
     * Get the settings: number of sample.
     *
     * @return
     */
    public int getNumberOfSample() {
        return this.nSample;
    }

    /**
     * Return exactly one random word from dictionary. If the dictionary is
     * empty, return null;
     */
    @Override
    public String getSampleWord() {
        if (this.candidates.isEmpty()) {
            return null;
        } else {
            Random rand = new Random(System.currentTimeMillis());
            // get N sample randomly
            int index = rand.nextInt(this.candidates.size());
            Iterator<String> iter = this.candidates.iterator();
            // seek the index s of cndidates
            for (int i = 0; i < index; i++) {
                iter.next();
            }
            return iter.next();
        }
    }

    /**
     * Return N arbitrary words in dictionary. N is defined by getNumberOfSample
     * method. If N is more than total number of words, return all words in the
     * dictionary.
     *
     * @return Random sample word of this dictionary.
     */
    @Override
    public Set<String> getSampleWords() {
        HashSet<String> samples = new HashSet<>();
        // get N sample randomly
        for (int s = 0; s < this.nSample && s < this.candidates.size(); ++s) {
            String randomSample;
            // This block just in case the random word already been selected
            // This is so that number of samples is nSample or candidates.size()
            do {
                randomSample = this.getSampleWord();
            } while (samples.contains(randomSample));
            // New word? Get in...
            samples.add(randomSample);
        }
        return samples;
    }

    /**
     * Return all words in this dictionary, sorted A-Z.
     *
     * @return
     */
    @Override
    public Set<String> getAllWords() {
        return new TreeSet<>(this.candidates);
    }

    /**
     * Print a message about this dictionary.
     *
     * @return
     */
    @Override
    public String information() {
        return "Dictionary from " + this.sourcePath;
    }

    /**
     * Check whether this word is present in the dictionary.
     *
     * @param word Word that wants to be searched.
     * @return True if word is present.
     */
    @Override
    public boolean contains(String word) {
        return this.candidates.contains(word);
    }

    @Override
    public Set<String> filterWordsStartingWith(char firstChar) {
        Set<String> allWord = this.getAllWords();
        Set<String> requestedWords = new HashSet<>();
        allWord.stream().filter((word) -> (word.startsWith("" + firstChar))).forEach((word) -> {
            requestedWords.add(word);
        });
        return requestedWords;
    }

    @Override
    public Set<String> filterWordsEndingWith(char lastChar) {
        Set<String> allWord = this.getAllWords();
        Set<String> requestedWords = new HashSet<>();
        allWord.stream().filter((word) -> (word.endsWith("" + lastChar))).forEach((word) -> {
            requestedWords.add(word);
        });
        return requestedWords;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this.getAllWords());
    }
}
